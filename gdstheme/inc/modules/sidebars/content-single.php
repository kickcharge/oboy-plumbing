<div id="sidebar">

	<aside class="widget menu-widget">
      <a class="parent">Recent Posts</a>
      <ul>
	  <?php
			$posts = get_posts(array(
				'post_type' => 'post',
				'posts_per_page' => 5,
				'post_parent' => null,
				'orderby' => 'date',
				'order'	=> 'DESC'
			));
			if( $posts ) { foreach( $posts as $post ) {
		?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		<?php wp_reset_postdata(); } } ?>
      </ul>
	</aside><!-- /.widget -->

	<aside class="widget menu-widget">
      <a class="parent">Categories</a>
      <ul>
        <?php
	        wp_list_categories(array(
	        	'show_option_all' => '',
	        	'title_li' =>'',
	        	'hide_empty' => 1,
	        	'depth' => 1,
	        ));
	    ?>
      </ul>
	</aside><!-- /.widget -->

	<aside class="widget">
		<?php echo get_search_form(); ?>
	</aside><!-- /.widget -->

	<?php
	$post_object = get_field('choose_your_cta');
	if( $post_object ):

	// override $post
	$post = $post_object;
	setup_postdata( $post );
	?>

	<aside class="widget text-widget">
	<div class="sidebar-testimonial call-to-action">
	    <div>
	      <?php the_post_thumbnail(); ?>
	    	<?php the_content(); ?>
	    </div>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	</div><!-- /.sidebar-testimonial -->
	</aside><!-- /.widget .text-widget -->
	<?php endif; ?>

</div><!-- /#sidebar -->
