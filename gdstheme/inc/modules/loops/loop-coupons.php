<div class="large-8 large-centered columns">
    <?php

      $coupons = new WP_Query(array(
        'post_type' => 'coupons',
        'post_parent' => 0
      ));

    ?>

    <?php while( $coupons->have_posts()) { $coupons->the_post(); ?>
      <div class="row">
        <div class="large-12 columns">
          <div class="coupon">
            <a href="<?php echo get_permalink(); ?>"><i class="fa fa-2x fa-print hide-for-print"></i></a>
            <div class="row">
              <div class="medium-7 columns">
                <h2><?php the_field('coupon_heading'); ?></h2>
                <h3><?php the_field('coupon_subheading'); ?></h3>
              </div>
              <div class="medium-5 columns">
                <?php the_post_thumbnail('full'); ?>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row">
              <div class="large-12 columns">
                <p><?php the_field('coupon_terms'); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php wp_reset_postdata(); ?>
</div>
