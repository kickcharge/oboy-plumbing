<section class="feed coupons section clearfix" data-field="<?php echo $field['key']; ?>">
    <div class="row">
    <div class="large-12 columns">
      <div class="row title-row">
        <div class="large-12 columns">
          <h3><?php the_sub_field('section_title_coupon'); ?></h3>
          <h4><?php the_sub_field('section_subtitle_coupon'); ?></h4>             
        </div>
      </div>
      <div class="row small-up-1 medium-up-2 large-up-3" data-equalizer data-equalize-by-row="true">
        <?php
          $posts = new WP_Query(array(
            'post_type' => 'coupons',
            'posts_per_page' => '3',
            'orderby' => 'date',
            'order' => 'ASC'
            )
          );
          while ( $posts->have_posts() ) : $posts->the_post();
        ?>
          <div class="column">
            <div class="callout" data-equalizer-watch>
              <div class="row">
                <div class="large-12 columns">
		          <div class="coupon">
			        <a href="<?php echo get_permalink(); ?>">	            
			            <div class="row">		              
		                <h5><?php the_field('coupon_heading'); ?></h5>
		                <h6><?php the_field('coupon_subheading'); ?></h6> 	              
			            </div>		            
			            <div class="row">
			              <div class="large-12 columns">
			                <p><?php the_field('coupon_terms'); ?></p>
			              </div>
			            </div>
			        </a>
		          </div>
		        </div>               
              </div>                  
            </div>
          </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>          
    </div>
  </div>
</section>