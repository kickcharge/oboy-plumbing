<?php
	if ( ! function_exists('case_studies') ) {

	// Register Custom Post Type
	function case_studies() {

		$labels = array(
			'name'                => _x( 'Case Studies', 'Post Type General Name', 'gdstheme' ),
			'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'gdstheme' ),
			'menu_name'           => __( 'Case Studies', 'gdstheme' ),
			'name_admin_bar'      => __( 'Case Studies', 'gdstheme' ),
			'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
			'all_items'           => __( 'All Case Studies', 'gdstheme' ),
			'add_new_item'        => __( 'Add New Case Study', 'gdstheme' ),
			'add_new'             => __( 'Add New', 'gdstheme' ),
			'new_item'            => __( 'New Case Study', 'gdstheme' ),
			'edit_item'           => __( 'Edit Case Study', 'gdstheme' ),
			'update_item'         => __( 'Update Case Study', 'gdstheme' ),
			'view_item'           => __( 'View Case Study', 'gdstheme' ),
			'search_items'        => __( 'Search Case Studies', 'gdstheme' ),
			'not_found'           => __( 'Not found', 'gdstheme' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
		);
		$rewrite = array(
			'slug'                => 'case-studies',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => __( 'case-studies', 'gdstheme' ),
			'description'         => __( 'This section is dedicated to creating case studies within your website.', 'gdstheme' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
			'taxonomies'          => array( 'category', 'post_tag' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-megaphone',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
		);
		register_post_type( 'case-studies', $args );

	}

	// Hook into the 'init' action
	add_action( 'init', 'case_studies', 0 );

	}

?>