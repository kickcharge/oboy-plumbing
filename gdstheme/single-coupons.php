<?php get_header(); ?>
<div class="content-container">
  <div class="row">
    <div class="large-8 large-centered columns">
      <div class="row">
        <div class="large-12 columns">
          <div class="coupon">
            <div class="row">
              <div class="small-7 columns">
                <h2><?php the_field('coupon_heading'); ?></h2>
                <h3><?php the_field('coupon_subheading'); ?></h3>
              </div>
              <div class="small-5 columns">
                <?php the_post_thumbnail('full'); ?>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row">
              <div class="large-12 columns">
                <p><?php the_field('coupon_terms'); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
