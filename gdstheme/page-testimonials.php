
<?php /* Template Name: Testimonial Page */ ?>
<?php get_header(); ?>

      <?php get_template_part('inc/modules/content', 'title'); ?>
      <div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
  			<div class="row">
  	      <div class="medium-8 columns">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			  		<?php the_content(); ?>
			<?php endwhile; ?>
			<?php else : ?>
			  	<h2>Sorry Nothing Found</h2>
			<?php endif; ?>
			<?php						
			
			$args = array( 
				'post_type' => 'testimonials', 
				'posts_per_page' => '-1', 
				'post_parent' => 0, 
				'orderby'=>'date',
				'order'=>'DESC', );						
			$myposts = get_posts( $args );
			foreach ( $myposts as $post ) : setup_postdata( $post ); ?>				
				
				<div class="testimonial">
					
					<?php the_content(); ?>
					<h5>- <?php the_title();?>, <?php the_field('location'); ?></h5>
										
				</div>						
				
			<?php endforeach; 
			wp_reset_postdata();?>
			
  	      </div>
          <?php get_sidebar('right'); ?>
          <?php get_template_part('inc/acf/page', 'builder'); ?>
        </div>
      </div>

<?php get_footer(); ?>
